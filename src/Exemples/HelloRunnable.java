package Exemples;

public class HelloRunnable implements Runnable {

    public void run() {
        System.out.println("Hell2o from a thread!");
    }

    public static void main(String args[]) {
        (new Thread(new HelloRunnable())).start();
    }

}