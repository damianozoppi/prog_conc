/**
 * A class to experiment Threads with matrix multiplication
 * Concurrent Programming - Assignment 2
 * 
 * @author      Jacques Supcik <jacques.supcik@hefr.ch>
 * @version     1.0                   
 * @since       2012-03-01
 *
 * Completion time: 1h
 *
 * Honor Code: I pledge that this program represents my own
 *   program code. I received help from no one in designing
 *   and debugging my program.
 *
 */

package tp02;

public class MatrixMultiplication {

    static int matrix_1 [][] = new int [][] {
            {3, 2, 1},
            {3, 3, 2},
            {7, 3, 1},
    };

    static int matrix_2 [][] = new int [][] {
            {1, 7, -4},
            {3, 0, 4},
            {1, 1, 8},
    };

    static int matrix_3 [][] = new int [3][3];

    
    private static class MM extends Thread {
        int x, y;

        MM(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public void run() {
            System.out.println("Thread " + x + " " + y + " Started");
            for (int i = 0; i < 3; i++) {
                System.out.println("Thread " + x + " " + y + " Running");
                Thread.yield(); // mette in pausa temporaneamente il thread corrente per
                				// permettere ad altri thread di essere eseguiti
                matrix_3[x][y] += matrix_1[x][i] * matrix_2[i][y];
            }
            System.out.println("Thread " + x + " " + y + " Done");
        }
    }

    public static void main(String[] args) {
        Thread threads [][] = new Thread [3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Thread t = new MM(i, j);
                t.start();
                threads[i][j] = t;
            }
        }

        for (Thread [] row : threads) { // come un foreach -> passa tutti gli elementi 
        								// della tabella senza per� permettere di trattare 
        								// gli stessi dato che non sono indexati
            for (Thread col : row) {
                try {
                    col.join();	// Il thread che esegue join() rimane cos� bloccato in attesa
                    			// della terminazione dell'altro thread
                } catch (InterruptedException e) {
                    System.out.println("I wasn't done!");
                }
            }
       }
       for (int [] row : matrix_3) {
            for (int col : row) {
                System.out.print(col + " ");
            }
            System.out.println();
        }
       System.out.println("DONE");
    }
}
