// la classe cr�mier est appel� du Pusher correspondent. Le cr�mier ajoute le
// beure et pr�pare le sandwich qui va manger. Enfin lib�re l'�picier pour qu'il puisse
// mettre � nouveau 2 ingr�dients sur la table.

package tp06;

public class Milkman implements Runnable {

 public void run() {
     while (true) {
         // wait for bread and ham
    	 try {
             SandwichGourmets.butterSem.acquire();
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
         System.out.println("Milkman adds butter and makes sandwich");
         SandwichGourmets.isBread = false;
         SandwichGourmets.isHam = false;
         SandwichGourmets.isButter = false;
         SandwichGourmets.grocer_semaphore.release();
         System.out.println("Milkman eats his sandwich");
     }
 }
}