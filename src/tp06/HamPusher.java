// Le pusher contr�le les ingredients pr�sents sur la table et
// en fonction de ces-ci va signaler la juste personne qui peut ajouter
// son ingr�dient et pr�parer le sandwich

package tp06;

public class HamPusher implements Runnable {
    
    public void run() {
        while (true) {
            try {
                SandwichGourmets.ham.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
            	SandwichGourmets.mutex.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        	if (SandwichGourmets.isBread) {
                SandwichGourmets.isBread = false;
                SandwichGourmets.butterSem.release();
            }
            else if (SandwichGourmets.isButter) {
                SandwichGourmets.isButter = false;
                SandwichGourmets.breadSem.release();
            }
            else
            	SandwichGourmets.isHam = true;
        	SandwichGourmets.mutex.release();
        }
    }
}