// Le pusher contr�le les ingredients pr�sents sur la table et
// en fonction de ces-ci va signaler la juste personne qui peut ajouter
// son ingr�dient et pr�parer le sandwich

package tp06;

public class BreadPusher implements Runnable {
    
    public void run() {
        while (true) {
            try {
                SandwichGourmets.bread.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
            	SandwichGourmets.mutex.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        	if (SandwichGourmets.isHam) {
                SandwichGourmets.isHam = false;
                SandwichGourmets.butterSem.release();
            }
            else if (SandwichGourmets.isButter) {
                SandwichGourmets.isButter = false;
                SandwichGourmets.hamSem.release();
            }
            else
            	SandwichGourmets.isBread = true;
        	SandwichGourmets.mutex.release();
        }
    }
}
