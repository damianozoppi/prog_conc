// Le pusher contr�le les ingredients pr�sents sur la table et
// en fonction de ces-ci va signaler la juste personne qui peut ajouter
// son ingr�dient et pr�parer le sandwich

package tp06;

public class ButterPusher implements Runnable {
    
    public void run() {
        while (true) {
            try {
                SandwichGourmets.butter.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
            	SandwichGourmets.mutex.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        	if (SandwichGourmets.isBread) {
                SandwichGourmets.isBread = false;
                SandwichGourmets.hamSem.release();
            }
            else if (SandwichGourmets.isHam) {
                SandwichGourmets.isHam = false;
                SandwichGourmets.breadSem.release();
            }
            else
            	SandwichGourmets.isButter = true;
        	SandwichGourmets.mutex.release();
        }
    }
}