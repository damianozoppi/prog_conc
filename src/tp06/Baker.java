// la classe bloulanger est appel� du Pusher correspondent. Le boulangier ajoute le
// pain et pr�pare le sandwich qui va manger. Enfin lib�re l'�picier pour qu'il puisse
// mettre � nouveau 2 ingr�dients sur la table.

package tp06;

public class Baker implements Runnable {

 public void run() {
     while (true) {
         // wait for ham and butter
    	 try {
             SandwichGourmets.breadSem.acquire();
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
         System.out.println("Baker adds bread and makes sandwich");
         SandwichGourmets.isBread = false;
         SandwichGourmets.isHam = false;
         SandwichGourmets.isButter = false;
         SandwichGourmets.grocer_semaphore.release();
         System.out.println("Baker eats his sandwich");
         
     }
 }

}