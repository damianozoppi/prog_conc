// la classe charcutier est appel� du Pusher correspondent. Le charcutier ajoute le
// jambon et pr�pare le sandwich qui va manger. Enfin lib�re l'�picier pour qu'il puisse
// mettre � nouveau 2 ingr�dients sur la table.

package tp06;

public class Butcher implements Runnable {

 public void run() {
     while (true) {
         // wait for bread and butter
    	 try {
             SandwichGourmets.hamSem.acquire();
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
         System.out.println("Butcher adds ham and makes sandwich");
         SandwichGourmets.isBread = false;
         SandwichGourmets.isHam = false;
         SandwichGourmets.isButter = false;
         SandwichGourmets.grocer_semaphore.release();
         System.out.println("Bucher eats his sandwich");
     }
 }
}