/**
 * Cette classe c'est la classe principale d'o� le programme d�marre.
 * Le programme consiste en un club de sandwich o� un �picier met 2
 * des 3 ing�dients sur la table et puis le juste employ� va rajouter
 * le 3�me pour pouvoir manger le sandwich. Il faut noter que le programme
 * fonctionne correctement mais il d�marre �trangement d'un point au milieu
 * du programme au lieu d'afficher au debut les premier deux ingredients
 * mis sur la table de l'�picier.
 * 
 * @author      Damiano Zoppi <damiano.zoppi@edu.hefr.ch>
 * @version     1.0                   
 * @since       2012-04-20
 *
 * Completion time: 4h
 *
 * Honor Code: Il s'agit de mon code avec l'aide du livre "the little book
 * of semaphore".
 *
 */

package tp06;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SandwichGourmets {
	public static Semaphore grocer_semaphore       = new Semaphore(1, false);

	public static Semaphore bread                  = new Semaphore(0, true);
	public static Semaphore ham                    = new Semaphore(0, true);
	public static Semaphore butter                 = new Semaphore(0, true);

	public static Semaphore breadSem               = new Semaphore(0, true);
	public static Semaphore hamSem                 = new Semaphore(0, true);
	public static Semaphore butterSem              = new Semaphore(0, true);

	public static Semaphore mutex                  = new Semaphore(1, true);

	public static boolean isBread, isButter, isHam = false;

	public static ExecutorService threadExecutor   = Executors.newCachedThreadPool();

	// point de d�part du programme
	public static void main(String[] args) {
		Grocer grocer                = new Grocer();
		Baker baker                  = new Baker();
		Butcher butcher              = new Butcher();
		Milkman milkman              = new Milkman();
		HamPusher hamPusher          = new HamPusher();
		ButterPusher butterPusher    = new ButterPusher();
		BreadPusher breadPusher      = new BreadPusher();
		threadExecutor.execute(grocer);
		threadExecutor.execute(baker);
		threadExecutor.execute(butcher);
		threadExecutor.execute(milkman);
		threadExecutor.execute(hamPusher);
		threadExecutor.execute(butterPusher);
		threadExecutor.execute(breadPusher);
	}
}