package tp03;

import tp03.barrier;

public class MatrixMultiplicationWithBarrier {

    static int matrix_1 [][] = new int [][] {
            {3, 2, 1},
            {3, 3, 2},
            {7, 3, 1},
    };

    static int matrix_2 [][] = new int [][] {
            {1, 7, -4},
            {3, 0, 4},
            {1, 1, 8},
    };

    static int matrix_3 [][] = new int [3][3];

    
    private static class MM extends Thread {
        int x, y;

        MM(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public void run() {
            System.out.println("Thread " + x + " " + y + " Started");
            for (int i = 0; i < 3; i++) {
                System.out.println("Thread " + x + " " + y + " Running");
                Thread.yield(); // mette in pausa temporaneamente il thread corrente per
                				// permettere ad altri thread di essere eseguiti
                matrix_3[x][y] += matrix_1[x][i] * matrix_2[i][y];
            }
            System.out.println("Thread " + x + " " + y + " Done");
            barrier.use();
        }
    }
    private static class MMPrint extends Thread {

        public void run() {
        	barrier.use();
	        for (int [] row : matrix_3) {
	            for (int col : row) {
	                System.out.print(col + " ");
	            }
	            System.out.println();
	        }
	       System.out.println("DONE");
        }
    }
        
    public static void main(String[] args) {
        Thread threads [][] = new Thread [3][3];
        barrier bar1 = new barrier(9);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Thread t = new MM(i, j);
                t.start();
                threads[i][j] = t;
            }
        }
        Thread print = new MMPrint();
        print.start();
    }
}

