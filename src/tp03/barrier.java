package tp03;

import java.util.concurrent.*;

public class barrier{
	
	static int n;
	static int count;
	static private Semaphore mutex;
	static private Semaphore bar;
	
	public barrier(int nbOfThreadToWait) {
		n = nbOfThreadToWait + 1;
		count = 0;
		mutex = new Semaphore(1);
		bar = new Semaphore(0);
	}
	
	public static void use() {
		try {
			mutex.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		count++;
		mutex.release();
		if (count == n) {
			bar.release();
		}
		try {
			bar.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		bar.release();
	}
}
