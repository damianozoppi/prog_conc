/**
 * A class to implement the philosopher in the dining philosopher problem.
 * Concurrent Programming - Assignment 2
 */

package tp03;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class Philosopher implements Runnable {

    static final int MAX_SLEEP_TIME = 2000;
    static final int MAX_EAT_TIME = 1000;
    
    static private Semaphore max_try = new Semaphore(4, true);

    int id = 0;
    Fork fork_left;
    Fork fork_right;
    
    static private Random randomGenerator = new Random();
    
    public Philosopher(int id, Fork fork_left, Fork fork_right) {
        this.id = id;
        this.fork_left = fork_left;
        this.fork_right = fork_right;
    }

    public void run() {
        while (true) {
            System.out.println("Philosopher " + this.id + " sleeps...");
            try {
                Thread.sleep(randomGenerator.nextInt(MAX_SLEEP_TIME));
            } catch (InterruptedException e) {};
            System.out.println("Philosopher " + this.id + " is hungry");
            try {max_try.acquire();} catch (InterruptedException e) {};
            fork_left.take(id);
            try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
            fork_right.take(id);
            System.out.println("Philosopher " + this.id + " eats...");
            try {
                Thread.sleep(randomGenerator.nextInt(MAX_EAT_TIME));
            } catch (InterruptedException e) {};
            fork_left.put(id);
            max_try.release();
            fork_right.put(id);
            
        }
    }
}


