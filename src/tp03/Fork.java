/**
 * A class to implement the fork in the dining philosopher problem.
 * Concurrent Programming - Assignment 2
 */

package tp03;

import java.util.concurrent.*;

public class Fork {
    
    private Semaphore fork;
    int id = 0;
    
    public Fork(int id) {
        this.fork = new Semaphore(1);
        this.id = id;
    }

    public void take(int philosopher) {
        System.out.println(
            "Philosopher " + philosopher + "   trying to take fork " + this.id
        );
        try {
            fork.acquire();
        } catch (InterruptedException e) {
        }
        System.out.println(
            "Philosopher " + philosopher + "   took fork " + this.id
        );
    }
    
    public void put(int philosopher) {
        System.out.println(
            "Philosopher " + philosopher + "   Putting fork " + this.id
        );
        fork.release();
    }
    
}


