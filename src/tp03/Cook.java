/** 
 * The Cook class implements the cook of the Dining Savage Problem
 */

package tp03;

import java.util.Random;

public class Cook implements Runnable {
    
    static final int MIN_COOK_TIME = 1000; // in milliseconds
    static final int MAX_COOK_TIME = 3000; // in milliseconds
    static private Random randomGenerator = new Random();
    
    public void run() {
        while (true) {
            System.out.println("Cook is sleeping");
            try {
                Pot.emptyPot.acquire();
            } catch (InterruptedException e) {
            }
            int cook_time = MIN_COOK_TIME
                + randomGenerator.nextInt(MAX_COOK_TIME - MIN_COOK_TIME);
            System.out.printf(
                "Cook is cooking for %.1f sec\n", cook_time/1000.0
            );
            try {
                Thread.sleep(cook_time);
            } catch (InterruptedException e) {};
            System.out.println("Food is ready");
            Pot.fullPot.release();
        }
    }
}